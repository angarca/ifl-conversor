#!/usr/bin/env ruby

def erasePath( str, path)
	if str[ 0 .. path.size-1] == path then
		str = str[ path.size .. -1]
	else
		puts 'Error: input line not match with path to erase.'
	end
	str
end

if ARGV.size < 3 then
	puts 'Usage: <input file> <output file> <fps on graphic editor> [<extra path, if necesary>]'
else

	output = File.open(ARGV[1],'w')
	if output == nil then
		puts "Error: output file '#{ARGV[1]}' can't be opened."
	else
		l = ''
		File.open(ARGV[0],'r')do|f|
			if f.gets.chomp("\n") != 'Randomize_graphics/SpriteIFL__1.0__' then puts 'Error: input file, invalid format.'
			else
				f.gets	# frames size, optimization to better load in c++, ignored
				while l=f.gets do if l.chomp! != '' then
					a = l.split(' ')
					a[1] = a[1].to_f * ARGV[2].to_f
					if a[1].to_i != a[1] then a[1] = a[1].to_i + 1 else a[1] = a[1].to_i end
					a[0] = erasePath( a[0], ARGV[3])
					output.puts "#{a[0]} #{a[1]}"
				end end
			end
		end
		output.close
	end

end

