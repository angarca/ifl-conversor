#!/usr/bin/env ruby

if ARGV.size < 3 then
	puts 'Usage: <input file> <output file> <fps on graphic editor> [<extra path, if necesary>]'
else

	output = File.open(ARGV[1],'w')
	if output == nil then
		puts "Error: output file '#{ARGV[1]}' can't be opened."
	else
		output.puts 'Randomize_graphics/SpriteIFL__1.0__'
		strOutputs = []
		l = ''
		File.open(ARGV[0],'r')do|f|while l=f.gets do if l.chomp! != '' then
			a = l.split(' ')
			if a.size < 2 then a[1] = '1' end
			a[1] = a[1].to_f / ARGV[2].to_f
			strOutputs[strOutputs.size] = "#{ARGV[3]}#{a[0]} #{a[1]}"
		end end end
		output.puts strOutputs.size, strOutputs
		output.close
	end

end

